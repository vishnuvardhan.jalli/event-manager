import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { EventFormComponent } from './event-form/event-form.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { PopoverModule } from 'ngx-bootstrap/popover';

import { AngularFontAwesomeModule } from 'angular-font-awesome';

import {MatButtonModule, MatCheckboxModule,MatChipsModule,MatIconModule,MatFormFieldModule,MatInputModule,MatRippleModule,MatRadioModule} from '@angular/material';
  


@NgModule({
  declarations: [
    AppComponent,
    EventFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule,
    BrowserAnimationsModule,
    TimepickerModule.forRoot(),PopoverModule.forRoot(),
    MatButtonModule, MatCheckboxModule, MatChipsModule,MatIconModule,MatFormFieldModule,MatInputModule,MatRippleModule,MatRadioModule,
    AngularFontAwesomeModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
