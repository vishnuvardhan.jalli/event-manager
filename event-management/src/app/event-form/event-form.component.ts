import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { Component, OnInit } from '@angular/core';
import {MatChipInputEvent} from '@angular/material';
import {ThemePalette} from '@angular/material/core';
export interface Guest {
  name: string;
}
export interface ChipColor {
  name: string;
  color: ThemePalette;
}

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.css']
})
export class EventFormComponent implements OnInit {
  //time = {hour: 13, minute: 30};
  model: any = {};
  timepickerVisible = false;
  time: Date;
  days: any[];
  guestsCan: any[];
  notTimes: any[];
  notifyTimeAdd: Date;
  notifyTimeAddEach: Date;
  eachModel: any;
  showGuestsError: boolean = false;
  //guests:ChipColor;
  constructor() { }

  ngOnInit() {
    this.model.days = [
      {displayName:'Mon',value:'mon',checked:true},
      {displayName:'Tue',value:'tue',checked:true},
      {displayName:'Wed',value:'wed',checked:true},
      {displayName:'Thu',value:'thu',checked:false},
      {displayName:'Fri',value:'fri',checked:true},
      {displayName:'Sat',value:'sat',checked:false},
      {displayName:'Sun',value:'sun',checked:false}
    ];
    this.model.guestsCan = [
      {displayName:'Modify event',value:'modify',checked:false},
      {displayName:'Invite Others',value:'invite',checked:false},
      {displayName:'See guest list',value:'see',checked:true},
    ];
    this.model.guests = [
      {name: 'vishnu.jalli@yatra.com', color: undefined},
      {name: 'vishnuvardhan.jalli@gmail.com', color: undefined},
      {name: 'abc', color : 'warn'},
    ];
    this.model.notTimes = ['2019-04-28T19:35:00.419Z'];
    //this.model.notificationType = 'sms';
  }

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  visibleNot = true;
  selectableNot = true;
  removableNot = true;
  addOnBlurNot = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  
  

  addGuest(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our guest
    if ((value || '').trim()) {
      this.model.guests.push({name: value.trim(),color: undefined});
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeGuest(g: Guest): void {
    const index = this.model.guests.indexOf(g);

    if (index >= 0) {
      this.model.guests.splice(index, 1);
    }
  }

  addNot(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.model.notTimes.push({name: value.trim()});
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeNot(notification): void {
    const index = this.model.notTimes.indexOf(notification);

    if (index >= 0) {
      this.model.notTimes.splice(index, 1);
    }
  }

  saveEvent(){
    console.log(this.areGuestsValid());
    if(this.areGuestsValid()){
      this.showGuestsError = true;
    }else{
      console.log(JSON.stringify(this.model));
    }
    
  }

  addNotify(){
    this.model.notTimes.push(this.notifyTimeAdd);
  }
  addNotifyEach(){
    this.model.notTimes[this.eachModel] = this.notifyTimeAddEach;
  }
  addEachToModel(not,i){
    this.eachModel = i;
    this.notifyTimeAddEach = not;
  }

  getColor(guest){
    console.log(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(guest.name))
    /*let styles = {
      'background-color': /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(guest.name) ? 'white' : 'red'
    };
    return styles;*/
    return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(guest.name)) ? '' : 'redBg';
  }
  areGuestsValid(){
    let valid = true;
    this.model.guests.forEach(guest => {
      if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(guest.name)) { valid = false } ;
    });
    return true;
  }
}
